package ua.ithillel.java.elementary.socket.http.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SimpleHttpClientImpl implements SimpleHttpClient {
    @Override
    public SimpleHttpResponse request(HttpMethod method, String url, String payload, Map<String, String> headers) {
        URI uri = URI.create(url);
        try (Socket socket = new Socket(uri.getHost(), uri.getPort())) {
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.print(method + " " + uri.getPath() + " HTTP/1.1\r\n");
            writer.print("Host: " + uri.getHost() + ":" + uri.getPort() + "\r\n");

            for (Map.Entry<String, String> entry : headers.entrySet()) {
                writer.print(entry.getKey() + ": " + entry.getValue() + "\r\n");
            }

            if (!method.getMethodName().equals("GET") && payload != null) {
                writer.print("Content-Length: " + payload.getBytes(StandardCharsets.UTF_8).length + "\r\n");
                writer.print("\r\n");
                writer.print(payload);
            } else {
                writer.print("\r\n");
            }

            writer.flush();

            Scanner sc = new Scanner(socket.getInputStream());

            String result = sc.nextLine();
            String[] s = result.split(" ");
            int statusCode = Integer.parseInt(s[1]);
            String statusText = s[2];

            Map<String, String> headersResponse = new HashMap<>();
            for (result = sc.nextLine(), s = result.split(": "); !result.isEmpty();
                 result = sc.nextLine(), s = result.split(": ")) {
                headersResponse.put(s[0], s[1]);
            }

            sc.nextLine();
            String payloadResponse = sc.nextLine();

            return new SimpleHttpResponse(statusCode, statusText, payloadResponse, headersResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
